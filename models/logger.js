// import dependencies
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const loggerSchema = new Schema({
    req: { type: String },
    status: { type: String },
    res: { type: String },
}, { timestamps: true });


const Logger = mongoose.model('logger', loggerSchema);

// exporting model
module.exports = Logger;